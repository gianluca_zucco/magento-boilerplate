<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Zeta_Theme_Block_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{
    public function removeLink($name)
    {
        unset($this->_links[$name]);
    }
}
