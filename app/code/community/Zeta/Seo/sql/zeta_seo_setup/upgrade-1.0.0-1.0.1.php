<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 *
 * @var $this Mage_Core_Model_Resource_Setup
 */

$installer = new Mage_Catalog_Model_Resource_Setup('core_setup');

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'is_noindex', array(
    'group' => Zeta_Seo_Helper_Data::CATEGORY_TAB_SEO,
    'label' => 'Is no-index',
    'input' => 'select',
    'type' => 'int',
    'source' => 'eav/entity_attribute_source_boolean',
    'default' => '0',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => false
));

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'is_nofollow', array(
    'group' => Zeta_Seo_Helper_Data::CATEGORY_TAB_SEO,
    'label' => 'Is no-follow',
    'input' => 'select',
    'type' => 'int',
    'source' => 'eav/entity_attribute_source_boolean',
    'default' => '0',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => false,
));
