<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project parmashop
 */
class Zeta_Seo_Helper_Product extends Mage_Core_Helper_Abstract
{
    public function getSnippetData(Mage_Catalog_Model_Product $product)
    {
        $_productData = array(
            'name' => $this->stripTags($product->getName()),
            'image' => Mage::helper('catalog/image')->init($product, 'small_image')->__toString(),
            'description' => $this->stripTags($product->getShortDescription()),
            'sku' => $product->getSku()
        );

        return array_merge(
            Mage::helper('zeta_seo')->getCommonSnippetInformation(),
            array_merge(
                $_productData,
                $this->getProductRatingData($product),
                $this->getProductCategoryData($product),
                $this->getProductPriceData($product)
            )
        );
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getProductCategoryData(Mage_Catalog_Model_Product $product)
    {
        try {
            $_categoryName = "";
            if (Mage::registry('current_category') instanceof Mage_Catalog_Model_Category) {
                $_categoryName = Mage::registry('current_category')->getName();
            } elseif (count($product->getCategoryIds())) {
                $_categoryNames = Mage::getResourceModel('catalog/category_collection')
                    ->addFieldToFilter('entity_id', array('in' => $product->getCategoryIds()))
                    ->addFieldToFilter('level', 2)
                    ->addAttributeToSelect('name')
                    ->addOrder('position', Varien_Data_Collection::SORT_ORDER_ASC)
                    ->getColumnValues('name');

                $_categoryName = reset($_categoryNames);
            } else {
                return array();
            }

            return array(
                'category' => $_categoryName
            );
        } catch (Exception $e) {
            return array();
        }
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getProductPriceData(Mage_Catalog_Model_Product $product)
    {
        return array(
            'offers' => array(
                "@type" => "AggregateOffer",
                "priceCurrency" => Mage::app()->getStore()->getCurrentCurrency()->getCode(),
                "price" => Mage::helper('core')->currency($product->getPrice(), false, false),
                "lowPrice" => Mage::helper('core')->currency($product->getFinalPrice(), false, false),
                "availability" => "http://schema.org/" . $product->isSaleable() ? "InStock" : "OutOfStock"
            )
        );
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getProductRatingData(Mage_Catalog_Model_Product $product)
    {
        Mage::getModel('review/review')->getEntitySummary($product, Mage::app()->getStore()->getId());

        $_ratingSummary = $product->getRatingSummary()->getRatingSummary();
        $_reviewsCount = $product->getRatingSummary()->getReviewsCount();

        if ($_ratingSummary) {
            return array(
                'aggregateRating' => array(
                    '@type' => 'AggregateRating',
                    'ratingValue' => (float)$_ratingSummary,
                    'reviewCount' => (int)$_reviewsCount,
                    'bestRating' => "100",
                    'worstRating' => "0"
                )
            );
        }
        return array();
    }
}
