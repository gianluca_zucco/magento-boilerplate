<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

class Zeta_Seo_Model_Resource_Redirect extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('zeta_seo/redirect', 'redirect_id');
    }
}
