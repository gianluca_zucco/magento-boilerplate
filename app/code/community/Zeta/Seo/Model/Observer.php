<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */
class Zeta_Seo_Model_Observer
{

    /**
     * Redirect if store code is missing
     *
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Exception
     */
    public function redirectToBaseStoreView(Varien_Event_Observer $observer)
    {
        /** @var Mage_Cms_IndexController $action */
        $action = $observer->getControllerAction();
        $pathInfo = ltrim($action->getRequest()->getRequestUri(), '/');
        $currentStoreCode = Mage::app()->getStore()->getCode();
        if (!preg_match("#^$currentStoreCode#", $pathInfo) && $pathInfo == '' && Mage::getStoreConfig('web/url/use_store')) {
            $action->getResponse()->setRedirect(Mage::app()->getStore()->getBaseUrl(), 301);
            $action->setFlag('', $action::FLAG_NO_DISPATCH, true);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function patch404(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Controller_Front_Action $action */
        $action = $observer->getControllerAction();
        $pathInfo = ltrim($action->getRequest()->getPathInfo(), "/");

        $redirect = Mage::getResourceModel('zeta_seo/redirect_collection')
            ->addFieldToFilter('redirect_from', $pathInfo)
            ->getFirstItem();

        if ($redirect->getId()) {
            $action->getResponse()->setRedirect($redirect->getRedirectTo(), (int) $redirect->getType());
            $action->setFlag('', Mage_Core_Controller_Front_Action::FLAG_NO_DISPATCH, true);
        }

    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function addSeoFields(Varien_Event_Observer $observer)
    {
        /** @var Varien_Data_Form $form */
        $form = $observer->getForm();
        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $form->getElement('base_fieldset');

        $fieldset->addField('seo_identifier', 'text', array(
            'name' => 'seo_identifier',
            'label' => Mage::helper('zeta_seo')->__('SEO Identifier'),
            'after_element_html' => sprintf('<pre>%s</pre>', Mage::helper('zeta_seo')->__('Connect CMS pages across languages')),
            'required' => false
        ));
    }

    /**
     * Since product collection can come both from default or layer, we just put the result into registry for further usage
     *
     * @param Varien_Event_Observer $observer
     */
    public function onProductCollectionLoadAfter(Varien_Event_Observer $observer)
    {
        $actionName = Mage::app()->getFrontController()->getAction()->getFullActionName();
        if ($actionName != 'catalog_category_view') return;
        if (Mage::registry('current_collection'))  {
            Mage::unregister('current_collection');
        }
        Mage::register('current_collection', $observer->getCollection());
    }
}
