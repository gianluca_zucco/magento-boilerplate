<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project parmashop
 */

class Zeta_Seo_Block_Category_Snippet extends Mage_Core_Block_Template
{
    public function getJsonSnippet()
    {
        return Mage::helper('core')->jsonEncode($this->_getSnippetData());
    }

    /**
     * Collection is defined into observer method
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|Varien_Data_Collection
     */
    public function getCollection()
    {
        $_collection = Mage::registry('current_collection');
        return $_collection instanceof Mage_Catalog_Model_Resource_Product_Collection ? $_collection : new Varien_Data_Collection();
    }

    protected function _getSnippetData()
    {
        return array(
            "@context" => "http://schema.org",
            "@type" => "WebPage",
            "breadcrumb" => $this->_getBreadCrumb(),
            "name" => $this->getCategory()->getName(),
            "aggregateRating" => $this->_getAggregateRating(),
        );
    }

    protected function _getAggregateRating()
    {
        $_aggregateRatingData = array(
            "@type" => "AggregateRating",
            "bestRating" => "100",
            "worstRating" => "0"
        );

        $_ratingValue = 0;
        $_reviewCount = 0;

        foreach ($this->getCollection() as $product)    {
            if (!$product->getRatingSummary() instanceof Mage_Review_Model_Review_Summary)    {
                Mage::getModel('review/review')->getEntitySummary($product, Mage::app()->getStore()->getId());
            }
            /** @var Mage_Review_Model_Review_Summary $_ratingSummary */
            $_ratingSummary = $product->getRatingSummary();
            $_ratingValue += $_ratingSummary->getRatingSummary();
            $_reviewCount += $_ratingSummary->getReviewsCount();
        }

        $_aggregateRatingData['ratingValue'] = ceil($_ratingValue / $this->getCollection()->count());
        $_aggregateRatingData['reviewCount'] = $_reviewCount;
        return $_aggregateRatingData;
    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        return Mage::registry('current_category');
    }

    protected function _getBreadCrumb()
    {
        $_names = Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToSelect('name')
            ->addFieldToFilter('level', array('gteq' => 2))
            ->addFieldToFilter('entity_id', array('in' => $this->getCategory()->getPathIds()))
            ->setOrder('level', Varien_Data_Collection::SORT_ORDER_ASC)
            ->getColumnValues('name');

        return implode(' > ', $_names);
    }
}
