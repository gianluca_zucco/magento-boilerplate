<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

class Zeta_Seo_Block_Category_Meta extends Zeta_Seo_Block_Abstract
{
    public function getAlternateLink(Mage_Core_Model_Store $store)
    {
        $category = $this->getCategory();
        /** @var Mage_Core_Model_Url_Rewrite $url */
        $url = Mage::getModel('core/url_rewrite')->setStoreId($store->getId())->loadByIdPath('category/' . $category->getId());
        return $store->getUrl('/', array('_direct' => $url->getRequestPath()));
    }

    /**
     * Set in cache for 24 hrs
     *
     * @return int
     */
    public function getCacheLifetime()
    {
        return 86400;
    }

    /**
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(parent::getCacheTags(), array('catalog_category'));
    }

    public function getCacheKeyInfo()
    {
        return array_merge(parent::getCacheKeyInfo(), array($this->getCategory()->getId()));
    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        $category = Mage::registry('current_category');
        return $category;
    }

    /**
     * @return null|string
     * @throws Exception
     */
    public function getPrevLink()
    {
        $varName = $this->getPageVarName();
        $currentPage = $this->getRequest()->getParam($varName, false);
        if ($currentPage >= 2) {
            if ($currentPage == 2) {
                return $this->getUrl('/', array(
                    '_direct' => $this->getCategory()->getUrlPath()
                ));
            } else {
                return $this->getUrl('/', array(
                    '_direct' => $this->getCategory()->getUrlPath(),
                    '_query' => array(
                        $varName => $currentPage - 1,
                    )
                ));
            }
        }
        return null;
    }

    /**
     * @return string
     */
    protected function getPageVarName()
    {
        $varName = 'p';
        return $varName;
    }

    /**
     * @return bool
     */
    public function isNoIndex()
    {
        return (bool) $this->getCategory()->getIsNoindex();
    }

    /**
     * @return bool
     */
    public function isNoFollow()
    {
        return (bool) $this->getCategory()->getIsNofollow();
    }
}
