<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @method bool|null getIsHomepage()
 */

class Zeta_Seo_Block_Abstract extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        return Mage::helper('zeta_seo')->isEnabled() ? parent::_toHtml() : '';
    }
}
