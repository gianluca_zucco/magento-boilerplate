<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

class Zeta_Seo_Block_Homepage_Meta extends Zeta_Seo_Block_Abstract
{
    public function getAlternateLink(Mage_Core_Model_Store $store)
    {
        return $store->getBaseUrl();
    }
}
