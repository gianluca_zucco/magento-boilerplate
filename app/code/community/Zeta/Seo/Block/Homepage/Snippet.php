<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

class Zeta_Seo_Block_Homepage_Snippet extends Zeta_Seo_Block_Abstract
{

    public function getJsonSnippet()
    {
        $_data = array(
            '@type' => "Website",
            'url' => Mage::app()->getStore()->getBaseUrl(),
            'name' => Mage::getStoreConfig('design/head/default_title'),
            'description' => Mage::getStoreConfig('design/head/default_description'),
            'potentialAction' => array(
                '@type' => 'SearchAction',
                'target' => $this->getUrl('catalogsearch/result', array(
                    'q' => '{search_term}'
                )),
                'query-input' => "required name=search_term"
            )
        );

        return Mage::helper('core')->jsonEncode(array_merge(Mage::helper('zeta_seo')->getCommonSnippetInformation(), $_data));
    }
}
