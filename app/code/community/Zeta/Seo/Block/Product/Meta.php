<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

class Zeta_Seo_Block_Product_Meta extends Zeta_Seo_Block_Product_Abstract
{
    public function getAlternateLink(Mage_Core_Model_Store $store)
    {
        $product = $this->getProduct();
        /** @var Mage_Core_Model_Url_Rewrite $url */
        $url = Mage::getModel('core/url_rewrite')->setStoreId($store->getId())->loadByIdPath('product/' . $product->getId());
        return $store->getUrl('/', array('_direct' => $url->getRequestPath()));
    }
}
