<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

abstract class Zeta_Seo_Block_Product_Abstract extends Zeta_Seo_Block_Abstract
{
    /**
     * Set in cache for 24 hrs
     *
     * @return int
     */
    public function getCacheLifetime()
    {
        return 86400;
    }

    /**
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(parent::getCacheTags(), array('catalog_product'));
    }

    public function getCacheKeyInfo()
    {
        return array_merge(parent::getCacheKeyInfo(), array($this->getProduct()->getId()));
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        $product = Mage::registry('current_product');
        return $product;
    }
}
