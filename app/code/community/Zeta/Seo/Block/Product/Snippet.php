<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

class Zeta_Seo_Block_Product_Snippet extends Zeta_Seo_Block_Product_Abstract
{

    public function getJsonSnippet()
    {
        return Mage::helper('core')->jsonEncode($this->_getSnippetData());
    }

    protected function _getSnippetData()
    {
        return Mage::helper('zeta_seo/product')->getSnippetData($this->getProduct());
    }
}
