<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Teaser extends Mzentrale_Widget_Block_Abstract
{
    protected $_template = 'mzentrale/widget/teaser.phtml';

    public function getContentContainerCssClass()
    {
        return sprintf('background-color: %s; color: %s; text-align: %s;',
            $this->getLayerBackgroundColor(),
            $this->getTextColor(),
            $this->getTextAlignment()
        );
    }
}
