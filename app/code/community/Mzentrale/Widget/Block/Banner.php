<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 * @method string getAltText()
 */

class Mzentrale_Widget_Block_Banner extends Mzentrale_Widget_Block_Abstract
{
    protected $_template = 'mzentrale/widget/banner.phtml';
}
