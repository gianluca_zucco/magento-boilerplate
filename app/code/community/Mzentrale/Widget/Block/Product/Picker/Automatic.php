<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project parmashop
 */
class Mzentrale_Widget_Block_Product_Picker_Automatic extends Mzentrale_Widget_Block_Product_Picker
{
    public function getProductCollection()
    {
        if (!$this->_productCollection) {
            $this->_productCollection = Mage::helper('mzentrale_widget')->getTodayHighlightCollection();
        }
        return $this->_productCollection;
    }
}
