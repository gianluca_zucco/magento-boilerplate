<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 * @method string getProducts()
 */

class Mzentrale_Widget_Block_Product_Teaser extends Mzentrale_Widget_Block_Product
{
    protected $_template = 'mzentrale/widget/product/teaser.phtml';
    protected $_productCollection = array();

    public function getProductCollection()
    {
        if (!$this->_productCollection) {
            $productSkus = $this->getProducts();

            if ($productSkus)   {

                $productSkus = explode(',', $productSkus);
                $fallbackCount = $this->getCount($productSkus);

                /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
                $collection = Mage::getResourceModel('catalog/product_collection')
                    ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes())
                    ->addFieldToFilter('sku', array('in' => $productSkus));
                $collection->load();
                if ($fallbackCount) {

                    $_fallbackCollection = $this->_getFallbackCollection();
                    if ($_fallbackCollection)   {
                        foreach ($_fallbackCollection as $product) {
                            try {
                                $collection->addItem($product);
                            } catch (Exception $e)  {
                                continue;
                            }
                        }
                    }
                }
                $this->_productCollection = $collection;
            } else {
                $_fallbackCollection = $this->_getFallbackCollection($this->getMaximumCount());
                if ($_fallbackCollection)   {
                    $this->_productCollection = $_fallbackCollection;
                }
            }
        }
        return $this->_productCollection;
    }

    /**
     * @param array $productSkus
     * @return int
     */
    public function getCount(array $productSkus = array())
    {
        $maximum = $this->getMaximumCount();
        return $maximum - count($productSkus);
    }



    /**
     * @return Mage_Catalog_Model_Category
     */
    public function getFallBackCategory()
    {
        preg_match('/category\/([\d]+)/', $this->getData('category'), $matches);
        $id = end($matches);
        return Mage::getModel('catalog/category')->load($id);
    }

    /**
     * @return mixed
     */
    public function getMaximumCount()
    {
        $maximum = Mage::getStoreConfig('mzentrale_widget/product_teaser/count');
        return $maximum;
    }

    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function _getFallbackCollection()
    {
        $fallBackCategory = $this->getFallBackCategory();
        if ($fallBackCategory->getId()) {
            $_fallbackCollection = $fallBackCategory->getProductCollection()
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes())
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->addOrder('news_to_date', 'DESC');

            $_fallbackCollection->load();
            return $_fallbackCollection;
        }
    }

    public function getTextAlignment($fieldId = 'text_alignment')
    {
        return Mzentrale_Widget_Model_Source_Alignment::ALIGN_CENTER;
    }

    public function getProductColumnCssClass()
    {
        $count = max(count($this->getProductCollection()), 1);
        return sprintf('col-xs-%s', 12/$count);
    }
}
