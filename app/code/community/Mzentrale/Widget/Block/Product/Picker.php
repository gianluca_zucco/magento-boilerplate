<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 */

class Mzentrale_Widget_Block_Product_Picker extends Mzentrale_Widget_Block_Product
{
    protected $_template = 'mzentrale/widget/product/picker.phtml';
    protected $_productCollection = array();

    public function getProductCollection()
    {
        if (!$this->_productCollection) {
            $productSkus = $this->getProducts();
            if ($productSkus)   {
                $productSkus = explode(',', $productSkus);
                /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
                $collection = Mage::getResourceModel('catalog/product_collection')
                    ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes())
                    ->addFieldToFilter('sku', array('in' => $productSkus))
                    ->setPage(0, $this->getMaximumCount());
                $this->_productCollection = $collection->load();
            } else {
                $_fallbackCollection = $this->_getFallbackCollection($this->getMaximumCount());
                if ($_fallbackCollection)   {
                    $this->_productCollection = $_fallbackCollection;
                }
            }
        }
        return $this->_productCollection;
    }

    public function getTextAlignment($fieldId = 'text_alignment')
    {
        return Mzentrale_Widget_Model_Source_Alignment::ALIGN_CENTER;
    }

    public function getProductColumnCssClass()
    {
        $count = max(count($this->getProductCollection()), 1);
        return sprintf('col-xs-%s', 12/$count);
    }

    /**
     * Get selected product SKU
     *
     * @return string
     */
    public function getProducts()
    {
        $products = $this->getData('products');
        preg_match('/product\/(.+)/', $products, $matches);
        $sku = trim(end($matches));
        return $sku;
    }
}
