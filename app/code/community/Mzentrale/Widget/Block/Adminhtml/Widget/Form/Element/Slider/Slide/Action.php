<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Slider_Slide_Action extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Slider_Element
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->_addImageField('image', 'Image');
        $this->_addImageField('mobile_image', 'Mobile Image');
        $this->_addField('promo_code', 'text', 'Promo code');
        $this->_addField('layer_color', 'text', 'Layer color', array(
            'description' => $this->__('Layer transparency is set at 60%.')
        ));
        $this->_addField('transparent_layer_background', 'select', 'Transparent background', array(
            'required' => false,
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        $this->_addImageField('layer_pattern', 'Layer pattern', array('required' => false));
        $this->_addField('text_color', 'text', 'Text color', array(
            'required' => false
        ));
        $this->_addField('button_text', 'text', 'Button text');
        $this->_addButtonLink('button_link', 'Button link');
        $this->_addField('small_text', 'text', 'Small text', array(
            'required' => false
        ));
        $this->_addField('small_text_background_color', 'text', 'Small text background color', array(
            'required' => false
        ));
        return $this;
    }

    public function getFrontendBlockType()
    {
        return 'mzentrale_widget/slider_slide_action';
    }
}
