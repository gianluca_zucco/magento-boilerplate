<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Banner extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->_addImageField('image', 'Image');
        $this->_addImageField('mobile_image', 'Mobile image');
        $this->_addField('alt_text', 'text', 'Alt text');
        $this->_addButtonLink('button_link', 'Banner link', array('required' => false));

        return $this;
    }
}
