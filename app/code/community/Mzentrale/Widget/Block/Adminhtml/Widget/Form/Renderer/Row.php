<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Renderer_Row extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Renderer_Abstract
{
    public function getAvailableElementsTemplate()
    {
        return array_keys(Mage::helper('mzentrale_widget')->getConfiguredAvailableElements());
    }

    /**
     * Returns form template html
     *
     * @param null $config
     * @return string
     */
    public function getFormTemplateHtml($config = null)
    {
        $configuredBlock = Mage::getStoreConfig("mzentrale_widget/available_row_elements/$config/block");
        if (!$configuredBlock)  {
            Mage::throwException('Missing configuration for block code ' . $config);
        }
        return Mage::getSingleton('core/layout')->createBlock($configuredBlock)->toHtml();
    }

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->toHtml();
    }
}
