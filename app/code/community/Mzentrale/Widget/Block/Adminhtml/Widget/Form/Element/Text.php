<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Text extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    public function _prepareForm()
    {
        parent::_prepareForm();
        $this->_addField('use_static_block', 'select', 'Use static block', array(
            'required' => true,
            'values' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        $this->_addStaticBlockLink('static_block', 'Static block', array('required' => false));
        $this->_addTextArea('html', 'Text/HTML', array('required' => false, 'class' => ''));
    }
}
