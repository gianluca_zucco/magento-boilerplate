<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Teaser extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    public function _prepareForm()
    {
        parent::_prepareForm();
        $this->_addImageField('image', 'Image');
        $this->_addImageField('mobile_image', 'Mobile Image #1');
        $this->_addField('title', 'text', 'Title');
        $this->_addTextArea('text', 'Text');
        $this->_addField('text_color', 'text', 'Text color');
        $this->_addField('layer_color', 'text', 'Background color', array('required' => false));
        $this->_addField('transparent_layer_background'  . $this->getCode(), 'select', 'Transparent background', array(
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        $this->_addField('text_alignment'  . $this->getCode(), 'select', 'Text alignment', array(
            'values' => Mage::getSingleton('mzentrale_widget/source_alignment')->toOptionArray()
        ));
        $this->_addField('button_text', 'text', 'Button text');
        $this->_addButtonLink('button_link', 'Button link');
    }
}
