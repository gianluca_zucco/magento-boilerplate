<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Outfit extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->_addField('title', 'text', 'Headline', array('required' => false));
        $this->_addField('text', 'text', 'text', array('required' => false));
        $this->_addImageField('main_image', 'Main image');
        $this->_addImageField('main_mobile_image', 'Main mobile image');
        $this->_addProductPicker('products', 'Pick products');
        $this->_addField('button_text', 'text', 'Button text');
        $this->_addButtonLink('button_link', 'Button & Image link');
    }
}
