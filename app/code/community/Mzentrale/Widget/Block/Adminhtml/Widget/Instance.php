<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Instance extends Mage_Widget_Block_Adminhtml_Widget_Instance
{
    /**
     * Block constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'mzentrale_widget';
        $this->_controller = 'adminhtml_widget_instance';
        $this->_headerText = Mage::helper('widget')->__('Manage Widget Instances');
        $this->_updateButton('add', 'label', Mage::helper('widget')->__('Add New Widget Instance'));
    }
}
