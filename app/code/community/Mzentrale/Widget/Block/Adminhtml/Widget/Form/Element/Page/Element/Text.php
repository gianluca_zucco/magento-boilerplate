<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Text extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element implements Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Interface
{
    public function prepareFields()
    {
        $this->_addField('use_static_block'.$this->getCode(), 'select', 'Use static block', array(
            'required' => true,
            'values' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        $this->_addStaticBlockLink('static_block'.$this->getCode(), 'Static block', array('required' => false));
        $this->_addTextArea('html'.$this->getCode(), 'Text/HTML', array('required' => false, 'class' => ''));
    }
}
