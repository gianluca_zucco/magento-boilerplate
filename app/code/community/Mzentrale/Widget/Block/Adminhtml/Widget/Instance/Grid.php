<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid extends Mage_Widget_Block_Adminhtml_Widget_Instance_Grid
{
    /**
     * Prepare grid columns
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Grid
     */
    protected function _prepareColumns()
    {
        $addAfter = 'sort_order';
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumnAfter('store_ids', array(
                'header'    =>  Mage::helper('mzentrale_widget')->__('Stores'),
                'width'     =>  '100',
                'align'     =>  'center',
                'index'     =>  'store_ids',
                'type'      =>  'store'
            ), $addAfter);
            $addAfter = 'store_ids';
        }

        $this->addColumnAfter('is_valid', array(
            'header'    =>  Mage::helper('mzentrale_widget')->__('Validity'),
            'width'     =>  '100',
            'align'     =>  'center',
            'index'     =>  '',
            'renderer'  => 'Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer_Date'
        ), $addAfter);

        $this->addColumnAfter('copy', array(
            'header'    =>  Mage::helper('mzentrale_widget')->__('Copy to'),
            'align'     =>  'center',
            'renderer'  =>  'Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer_Copy'
        ), 'is_valid');

        $this->addColumnAfter('delete', array(
            'header'    =>  Mage::helper('mzentrale_widget')->__('Delete'),
            'width'     =>  '100',
            'align'     =>  'center',
            'renderer'  =>  'Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer_Delete'
        ), 'copy');

        return parent::_prepareColumns();
    }

}
