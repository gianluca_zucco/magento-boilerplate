<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Product_Teaser extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element implements Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Interface
{
    public function prepareFields()
    {
        $this->_addProductPicker('products' . $this->getCode(), 'Pick products', array('required' => false));
    }
}
