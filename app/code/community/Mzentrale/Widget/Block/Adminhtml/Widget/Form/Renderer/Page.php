<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Renderer_Page extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Renderer_Abstract
{
    public function getAvailableElementsTemplate()
    {
        return array_keys(Mage::helper('mzentrale_widget')->getConfiguredPageAvailableElements());
    }

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->toHtml();
    }

    /**
     * Returns form template html
     *
     * @param null $config
     * @return string
     */
    public function getFormTemplateHtml($config = null)
    {
        $configuredBlock = Mage::getStoreConfig("mzentrale_widget/available_page_elements/$config/block");
        if (!$configuredBlock)  {
            Mage::throwException('Missing configuration for block code ' . $config);
        }
        if (!is_object(Mage::getSingleton('core/layout')->createBlock($configuredBlock)))   {
            Mage::throwException(sprintf('%s class does not exists.', $configuredBlock));
        }

        $childConfig = Mage::getStoreConfig("mzentrale_widget/available_page_elements/$config/children");
        return Mage::getSingleton('core/layout')
            ->createBlock($configuredBlock)
            ->setConfiguredChildren($childConfig)
            ->toHtml();
    }
}
