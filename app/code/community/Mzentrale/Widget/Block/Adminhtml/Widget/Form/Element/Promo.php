<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Promo extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->_addImageField('image', 'Image', array('required' => false));
        $this->_addImageField('mobile_image', 'Mobile image', array('required' => false));
        $this->_addField('use_static_block', 'select', 'Use static block', array(
            'required' => true,
            'values' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        $this->_addStaticBlockLink('static_block', 'Static block', array('required' => false));
        $this->_addFullOptionLink('link', 'Link', array('required' => false));
    }
}
