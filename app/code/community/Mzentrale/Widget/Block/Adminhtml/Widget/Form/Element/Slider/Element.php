<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Slider_Element extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->getFieldset()->removeField('#{field_prefix}_columns');
        $this->_addDateField('valid_from', 'Valid from', array('required' => false));
        $this->_addDateField('valid_to', 'Valid to', array('required' => false));
        $this->_addField('cache_lifetime', 'hidden', '', array('value' => 86400));
    }
}
