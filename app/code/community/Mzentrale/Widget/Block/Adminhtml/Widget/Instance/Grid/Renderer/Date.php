<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer_Date extends Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer
{
    protected $_template = 'mzentrale/widget/instance/grid/renderer/date.phtml';
    const VALID = 'valid';
    const INVALID = 'invalid';
    const WARNING = 'warning';

    /**
     * @param Mage_Core_Model_Store $store
     * @return bool
     */
    public function filter($store)
    {
        return !in_array($store->getId(), $this->getWidgetInstance()->getStoreIds());
    }

    public function getDate($index)
    {
        $data = $this->getWidgetInstance()->getWidgetParameters();
        if (array_key_exists($index, $data))    {
            $date = $data[$index];
            if ($date) {
                $date = Mage::getModel('core/locale')->storeDate(Mage_Core_Model_App::ADMIN_STORE_ID, $date, false);
                return Mage::getModel('core/date')->date('d/m/Y', $date);
            }
        }
        return null;
    }

    public function getValidity()
    {
        $parameters = $this->getWidgetInstance()->getWidgetParameters();
        if (!array_key_exists(Mzentrale_Widget_Model_Instance::WIDGET_CUSTOM_FIELDNAME, $parameters)) {
            return false;
        }
        $toDay = new Zend_Date(Mage::getModel('core/locale')->storeDate(Mage_Core_Model_App::ADMIN_STORE_ID));

        if ($this->getWidgetInstance()->getType() == 'mzentrale_widget/row')    {
            $validTo = new Zend_Date($this->getDate('valid_to'));
            return !$toDay->isLater($validTo) ? self::VALID : self::INVALID;
        } else {
            $childs = $parameters[Mzentrale_Widget_Model_Instance::WIDGET_CUSTOM_FIELDNAME];
            $return = self::VALID;
            foreach ($childs as $blockParameters)
            {
                $validTo = Mage::app()->getLocale()->storeDate(0, new Zend_Date($this->_getDateFromBlockParameters($blockParameters, 'valid_to')), false);
                if ($toDay->isLater($validTo))  {
                    $return = self::WARNING;
                    break;
                }
            }
            return $return;
        }

    }

    /**
     * @param $blockParameters
     * @param $index
     * @return string|null
     */
    protected function _getDateFromBlockParameters($blockParameters, $index)
    {
        return isset($blockParameters[$index]) ? ($blockParameters[$index] ? $blockParameters[$index] : null) : null;
    }
}
