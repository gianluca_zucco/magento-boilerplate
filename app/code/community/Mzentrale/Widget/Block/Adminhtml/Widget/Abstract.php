<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

abstract class Mzentrale_Widget_Block_Adminhtml_Widget_Abstract extends Mage_Widget_Block_Adminhtml_Widget_Options implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    const WIDGET_PARAMETER_SUFFIX = 'parameters';

    /**
     * Prepare Widget Options Form and values according to specified type
     *
     * widget_type must be set in data before
     * widget_values may be set before to render element values
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->setValues();
        return $this;
    }

    /**
     * Getter
     *
     * @return Mage_Widget_Model_Widget_Instance
     */
    public function getWidgetInstance()
    {
        return Mage::registry('current_widget_instance');
    }

    /**
     * Prepare block children and data.
     * Set widget type and widget parameters if available
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Properties
     */
    protected function _preparelayout()
    {
        $this->setWidgetType($this->getWidgetInstance()->getType())
            ->setWidgetValues($this->getWidgetInstance()->getWidgetParameters());
        return parent::_prepareLayout();
    }

    /**
     * Prepare field name as two level deep
     *
     * @param string $fieldName
     * @return string
     */
    protected function prepareFieldName($fieldName)
    {
        return $this->getForm()->addSuffixToName($this->getForm()->addSuffixToName($fieldName, $this->getId()), self::WIDGET_PARAMETER_SUFFIX);
    }

    public function setValues()
    {
        $parameters = $this->getWidgetInstance()->getWidgetParameters();
        if (is_array($parameters) && array_key_exists($this->getId(), $parameters)) {
            $this->getForm()->setValues($parameters[$this->getId()]);
        }
    }

    public function addFields()
    {
        $form = $this->getForm();
        $fieldset = $form->addFieldset($this->getId(), array(
            'legend' => $this->getTabTitle(),
            'header_bar' => $this->getFormButtonsBlock()
                ->setButtonLabel($this->_getButtonLabel())
                ->setAvailableElements($this->getAvailableElements())
                ->toHtml()
        ));
        $field = $fieldset->addField('pro_forma', 'text', array());
        $field->setRenderer($this->getRenderer());
        return $this;
    }

    /**
     * Returns renderer class
     *
     * @return string
     */
    public function getRenderer()
    {
        return new Mzentrale_Widget_Block_Adminhtml_Widget_Form_Renderer_Js_Templates_Row();
    }

    /**
     * @return array
     */
    protected function _getRendererFieldData()
    {
        return array(
            'label' => $this->__('Field label'),
            'name' => 'templates'
        );
    }



    /**
     * @return string
     */
    protected function _getButtonLabel()
    {
        return $this->__('Add new');
    }

    abstract public function getAvailableElements();

    /**
     * @param $suffix
     * @return string
     */
    public function getWysiwygUrl($suffix)
    {
        return $this->getUrl('*/cms_wysiwyg_images/index/', array(
            'target_element_id' => '#{field_prefix}_' . $suffix
        ));
    }

    /**
     * @return mixed
     */
    public function getFormButtonsBlock()
    {
        return Mage::getSingleton('core/layout')
            ->createBlock('mzentrale_widget/adminhtml_widget_form_button')
            ->setAvailableElements($this->getAvailableElements())->setTemplate('mzentrale/widget/form/button.phtml');
    }
}
