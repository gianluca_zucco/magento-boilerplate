<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Outfit extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element implements Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Interface
{
    public function prepareFields()
    {
        $this->_addField('title'.$this->getCode(), 'text', 'Headline', array('required' => false));
        $this->_addField('text'.$this->getCode(), 'text', 'text', array('required' => false));
        $this->_addImageField('main_image' . $this->getCode(), 'Main image');
        $this->_addImageField('main_mobile_image' . $this->getCode(), 'Main mobile image');
        $this->_addProductPicker('products' . $this->getCode(), 'Pick products');
        $this->_addField('button_text'.$this->getCode(), 'text', 'Button text');
        $this->_addButtonLink('button_link'.$this->getCode(), 'Button & Image link');
    }
}
