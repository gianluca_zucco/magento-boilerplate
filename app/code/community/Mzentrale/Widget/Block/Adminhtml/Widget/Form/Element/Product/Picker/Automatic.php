<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Product_Picker_Automatic extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        return $this;
    }
}
