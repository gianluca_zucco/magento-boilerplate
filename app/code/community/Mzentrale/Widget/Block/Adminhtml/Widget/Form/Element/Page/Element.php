<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 * @method getCode()
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->getFieldset()->removeField('#{field_prefix}_columns');
        $this->_addDateField('valid_from', 'Valid from', array('required' => false));
        $this->_addDateField('valid_to', 'Valid to', array('required' => false));
        $this->_addField('cache_lifetime', 'hidden', '', array('value' => 86400));
        if ($this->getConfiguredChildren())    {
            foreach ($this->getConfiguredChildren() as $code => $data)  {
                $this->setCode($code);
                $mainFieldset = $this->getFieldset();
                $mainFieldset->setClass('mainfieldset');
                $mainFieldset->setRenderer(Mage::getSingleton('core/layout')->createBlock('mzentrale_widget/adminhtml_widget_form_renderer_fieldset'));
                $currentFieldset = $this->getFieldset()->addFieldset('#{field_prefix}_' . $code, array(
                    'legend' => @$data['label'],
                    'class' => 'subfieldset'
                ));
                $currentFieldset->setRenderer(Mage::getSingleton('core/layout')->createBlock('mzentrale_widget/adminhtml_widget_form_renderer_fieldset'));
                $this->setFieldset($currentFieldset);

                Mage::getSingleton('core/layout')->createBlock($data['block'])
                    ->setCode($this->getCode())
                    ->setForm($this->getForm())->setFieldset($currentFieldset)->prepareFields();
                //Ripristinate original fieldset
                $this->setFieldset($mainFieldset);
            }
        }
        return $this;
    }

    /**
     * @param $name
     * @return string
     */
    protected function _generateFieldName($name)
    {
        return parent::_generateFieldName($this->getCode() ? $this->getForm()->addSuffixToName($this->getForm()->addSuffixToName($name, $this->getCode()), Mzentrale_Widget_Model_Instance::WIDGET_CUSTOM_FIELDNAME) : $name);
    }
}
