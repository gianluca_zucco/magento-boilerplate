<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Teaser_Image_Double extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element implements Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Interface
{
    public function prepareFields()
    {
        $this->_addField('divider_1'.$this->getCode(), 'text', 'Upper image', array(), Mage::getSingleton('core/layout')->createBlock('mzentrale_widget/adminhtml_widget_form_renderer_divider'));
        $this->_addImageField('image_1' . $this->getCode(), 'Image #1');
        $this->_addImageField('mobile_image_1' . $this->getCode(), 'Mobile Image #1');
        $this->_addField('alt_text_1'  . $this->getCode(), 'text', 'Alt text #1');
        $this->_addButtonLink('button_link_1'  . $this->getCode(), 'Link #1', array('required' => false));
        $this->_addField('divider_2'.$this->getCode(), 'text', 'Lower image', array(), Mage::getSingleton('core/layout')->createBlock('mzentrale_widget/adminhtml_widget_form_renderer_divider'));
        $this->_addImageField('image_2'  . $this->getCode(), 'Image #2');
        $this->_addImageField('mobile_image_2'  . $this->getCode(), 'Mobile Image #2');
        $this->_addField('alt_text_2'  . $this->getCode(), 'text', 'Alt text #2');
        $this->_addButtonLink('button_link_2'  . $this->getCode(), 'Link #2', array('required' => false));
    }
}
