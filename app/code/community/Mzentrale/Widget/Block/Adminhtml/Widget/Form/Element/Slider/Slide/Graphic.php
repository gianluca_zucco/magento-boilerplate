<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Slider_Slide_Graphic extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Slider_Element
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->_addImageField('image', 'Image');
        $this->_addImageField('mobile_image', 'Mobile Image');
        $this->_addField('title', 'text', 'Title');
        $this->_addTextArea('text', 'Text');
        $this->_addField('text_color', 'text', 'Text color', array(
            'required' => false
        ));
        $this->_addField('text_container_position', 'select', 'Text container position', array(
            'values' => Mage::getSingleton('mzentrale_widget/source_alignment')->toOptionArray()
        ));
        $this->_addField('transparent_layer_background', 'select', 'Transparent background', array(
            'required' => false,
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        $this->_addField('text_alignment', 'select', 'Text alignment', array(
            'values' => Mage::getSingleton('mzentrale_widget/source_alignment')->toOptionArray()
        ));
        $this->_addButtonLink('button_link', 'Link');
        $this->_addField('small_text', 'text', 'Small text', array(
            'required' => false
        ));
        $this->_addField('small_text_background_color', 'text', 'Small text background color', array(
            'required' => false
        ));
        return $this;
    }

    public function getFrontendBlockType()
    {
        return 'mzentrale_widget/slider_slide_graphic';
    }
}
