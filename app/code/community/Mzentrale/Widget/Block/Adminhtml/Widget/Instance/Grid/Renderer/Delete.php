<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer_Delete extends Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer
{
    protected $_template = 'mzentrale/widget/instance/grid/renderer/delete.phtml';

    public function getDeleteUrl()
    {
        return $this->getUrl('*/widget_instance/delete', array(
            'instance_id' => $this->getWidgetInstance()->getId()
        ));
    }
}
