<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Banner extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element implements Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Interface
{
    public function prepareFields()
    {
        $this->_addImageField('image'  . $this->getCode(), 'Image');
        $this->_addImageField('mobile_image'  . $this->getCode(), 'Mobile Image #1');
        $this->_addField('alt_text'  . $this->getCode(), 'text', 'Alt text');
        $this->_addButtonLink('button_link'  . $this->getCode(), 'Link', array('required' => false));
    }
}
