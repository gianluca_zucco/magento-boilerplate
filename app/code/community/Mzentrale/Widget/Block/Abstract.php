<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 * @method int|null getIsActive()
 * @method string|null getValidFrom()
 * @method string|null getValidTo()
 * @method string|null getColumns()
 *
 * @method string getTitle()
 * @method string getText()
 * @method string getButtonText()
 */

abstract class Mzentrale_Widget_Block_Abstract extends Mage_Core_Block_Template
{
    protected $_columnsClasses = array(
        2 => 'col-xs-2',
        3 => 'col-xs-3',
        4 => 'col-xs-4',
        6 => 'col-xs-6',
        8 => 'col-xs-8',
        12 => 'col-xs-12'
    );

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        return $this->_checkIsValid() ? parent::_toHtml() : '';
    }

    /**
     * @return string
     */
    protected function _checkIsValid()
    {
        $today = Mage::app()->getLocale()->date();
        $validFrom = Mage::app()->getLocale()->date($this->getValidFrom());
        $validTo = Mage::app()->getLocale()->date($this->getValidTo());
        return !($today->isEarlier($validFrom) || $today->isLater($validTo));
    }

    public function getButtonLink($fieldId = 'button_link')
    {
        if (!$this->getData($fieldId)) return "";

        //Category URL
        if (preg_match('/^category/', $this->getData($fieldId))) {
            preg_match('/category\/(\d+)/', $this->getData($fieldId), $matches);
            return Mage::getModel('catalog/category')->setId(end($matches))->getUrl();
        }

        //CMS url
        $pageId = $this->getData($fieldId);
        $urlKey = Mage::getModel('cms/page')->load($pageId)->getIdentifier();
        return $this->getUrl('', array('_direct' => $urlKey));

    }

    public function getImageUrl($fieldId = 'image')
    {
        return $this->_getImageUrl($fieldId);
    }

    public function getMobileImageUrl($fieldId = 'mobile_image')
    {
        return $this->_getImageUrl($fieldId);
    }

    /**
     * @param $fieldId
     * @return string
     */
    protected function _getImageUrl($fieldId)
    {
        $pieces = explode('media', $this->getData($fieldId));
        return Mage::app()->getStore()->getBaseUrl('media') . ltrim(end($pieces), '/');
    }

    /**
     * @return string
     */
    public function getColumnCssClass()
    {
        if (array_key_exists($this->getColumns(), $this->_columnsClasses))   {
            return sprintf('%s', $this->_columnsClasses[$this->getColumns()]);
        }
        return '';
    }

    /**
     * Return text color
     *
     * @param string $fieldId
     * @return mixed
     */
    public function getTextColor($fieldId = 'text_color')
    {
        $textColor = $this->getData($fieldId);
        return !preg_match('/^#([a-fA-F\d]{3}|[a-fA-F\d]{6})$/', $textColor) ? Mage::getStoreConfig('mzentrale_widget/slider/text_color') : $textColor;
    }

    /**
     * Return text color
     *
     * @param string $fieldId
     * @return mixed
     */
    public function getLayerBackgroundColor($fieldId = 'layer_color')
    {
        $transparentBackground = $this->getData('transparent_layer_background');

        if ($transparentBackground) {
            return 'transparent';
        }
        return $this->_getRgba($fieldId);
    }

    /**
     * @param string $fieldId
     * @return int|mixed|string
     */
    public function getTextContainerPosition($fieldId = 'text_container_position')
    {
        $data = $this->getData($fieldId);
        switch ($data) {
            default:
            case Mzentrale_Widget_Model_Source_Alignment::ALIGN_LEFT:
                $data = '5%';
            break;
            case Mzentrale_Widget_Model_Source_Alignment::ALIGN_CENTER:
                $data = '25%';
            break;
            case Mzentrale_Widget_Model_Source_Alignment::ALIGN_RIGHT:
                $data = '45%';
            break;
        }
        return $data;
    }

    public function getTextAlignment($fieldId = 'text_alignment')
    {
        return $this->getData($fieldId);
    }

    public function getContentContainerCssClass()
    {
        return sprintf('left: %s; background-color: %s; color: %s; text-align: %s;',
            $this->getTextContainerPosition(),
            $this->getLayerBackgroundColor(),
            $this->getTextColor(),
            $this->getTextAlignment()
            );
    }

    public function getSmallTextBackgroundColor($fieldId = 'small_text_background_color')
    {
        return !$this->getData($fieldId) ? 'transparent' : $this->_getRgba($fieldId);
    }

    /**
     * @param $fieldId
     * @return string
     */
    protected function _getRgba($fieldId, $trasparency = '.6')
    {
        $data = ltrim($this->getData($fieldId), '#');

        if (!preg_match('/^[a-fA-F\d]{6}$/', $data)) {
            $data = Mage::getStoreConfig('mzentrale_widget/slider/layer_color');
        }
        return sprintf('rgba(%s)', Mage::helper('mzentrale_widget')->toRgba($data, $trasparency));
    }

    /**
     * @param string $field_id
     * @return string
     */
    public function getAltText($field_id = 'alt_text')
    {
        return $this->escapeHtml($this->getData($field_id));
    }

    public function getCacheKeyInfo()
    {
        $parentKey = parent::getCacheKeyInfo();
        array_push($parentKey, $this->getNameInLayout());
        return $parentKey;
    }
}
