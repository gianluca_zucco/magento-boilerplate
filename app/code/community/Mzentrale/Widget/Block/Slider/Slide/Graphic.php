<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Slider_Slide_Graphic extends Mzentrale_Widget_Block_Slider_Slide_Abstract
{
    protected $_template = 'mzentrale/widget/slider/slide/graphic.phtml';
}
