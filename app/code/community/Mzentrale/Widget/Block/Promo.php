<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Promo extends Mzentrale_Widget_Block_Abstract
{
    protected $_template = 'mzentrale/widget/promo.phtml';

    /**
     * @return string
     */
    public function getStaticBlockHtml()
    {
        if ($this->getData('static_block')) {
            return Mage::getSingleton('core/layout')
                ->createBlock('cms/block')
                ->setBlockId($this->getData('static_block'))
                ->toHtml();
        }
        return '';
    }

    public function getButtonLink($fieldId = 'button_link')
    {
        if (!$this->getData($fieldId)) return "";

        //Category URL
        if (preg_match('/^category/', $this->getData($fieldId))) {
            preg_match('/category\/(\d+)/', $this->getData($fieldId), $matches);
            return Mage::getModel('catalog/category')->setId(end($matches))->getUrl();
        }

        //Product URL
        if (preg_match('/^product/', $this->getData($fieldId))) {
            preg_match('/product\/(\d+)/', $this->getData($fieldId), $matches);
            /** @var Mage_Catalog_Model_Product $product */
            $productId = Mage::getModel('catalog/product')->getIdBySku(end($matches));
            return Mage::getModel('catalog/product')->setId($productId)->getProductUrl();
        }

        //External URL
        if (preg_match('/^http/', $this->getData($fieldId))) {
            return $this->getData($fieldId);
        }

        //CMS url
        $pageId = $this->getData($fieldId);
        $urlKey = Mage::getModel('cms/page')->load($pageId)->getIdentifier();
        return $this->getUrl('', array('_direct' => $urlKey));
    }
}
