<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Text extends Mzentrale_Widget_Block_Abstract
{
    protected $_template = 'mzentrale/widget/text.phtml';

    /**
     * @return string
     */
    public function getStaticBlockHtml()
    {
        if ($this->getData('static_block')) {
            return Mage::getSingleton('core/layout')
                ->createBlock('cms/block')
                ->setBlockId($this->getData('static_block'))
                ->toHtml();
        }
        return '';
    }
}
