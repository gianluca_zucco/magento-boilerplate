<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Teaser_Image_Double extends Mzentrale_Widget_Block_Product_Teaser
{
    protected $_template = 'mzentrale/widget/teaser/double/image.phtml';
}
