<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project parmashop
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'today_highlight', array(
    'group' => 'General',
    'label' => 'In evidenza il',
    'type' => 'datetime',
    'input' =>  'date',
    'backend' => 'eav/entity_attribute_backend_datetime',
    'used_in_product_listing' => true,
    'used_for_promo_rules' => true,
    'visible_on_front' => false,
    'global'    =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'user_defined' => true,
    'default_value' => new Zend_Db_Expr('NULL'),
    'required' => false,
    'is_configurable' => false
));
