<?php

require_once Mage::getModuleDir('controllers', 'Mage_Widget') . '/Adminhtml/Widget/InstanceController.php';

class Mzentrale_Widget_Adminhtml_Widget_InstanceController extends Mage_Widget_Adminhtml_Widget_InstanceController
{
    /**
     * Init widget instance object and set it to registry
     *
     * @return Mage_Widget_Model_Widget_Instance|boolean
     */
    protected function _initWidgetInstance()
    {
        $this->_title($this->__('CMS'))->_title($this->__('Widgets'));

        /** @var $widgetInstance Mage_Widget_Model_Widget_Instance */
        $widgetInstance = Mage::getModel('mzentrale_widget/instance');

        $instanceId = $this->getRequest()->getParam('instance_id', null);
        $type       = $this->getRequest()->getParam('type', null);
        $package    = $this->getRequest()->getParam('package', null);
        $theme      = $this->getRequest()->getParam('theme', null);

        if ($instanceId) {
            $widgetInstance->load($instanceId);
            if (!$widgetInstance->getId()) {
                $this->_getSession()->addError(Mage::helper('widget')->__('Wrong widget instance specified.'));
                return false;
            }
        } else {
            $packageTheme = $package . '/' . $theme == '/' ? null : $package . '/' . $theme;
            $widgetInstance->setType($type)
                ->setPackageTheme($packageTheme);
        }
        Mage::register('current_widget_instance', $widgetInstance);
        return $widgetInstance;
    }

    public function copyAction()
    {
        $response = array(
            'status' => false,
            'message' => null
        );

        try {

            $storeId = $this->getRequest()->getParam('copy_to', false);
            $instanceId = $this->getRequest()->getParam('instance_id', false);

            if (!$this->getRequest()->isAjax()) {
                $this->_redirect('cms/index/noroute');
                return;
            }

            if (!$storeId)    {
                Mage::throwException($this->__('Store ID cannot be empty'));
            }

            if (!$instanceId)    {
                Mage::throwException($this->__('Widget instance to copy from must be specified'));
            }

            /** @var Mage_Widget_Model_Widget_Instance $widgetInstance */
            $widgetInstance = Mage::getModel('mzentrale_widget/instance')->load($instanceId);
            $widgetInstance->unsetData($widgetInstance->getIdFieldName());
            $widgetInstance->setStoreIds($storeId);

            $groups = array();
            if (is_array($widgetInstance->getData('page_groups'))) {
                foreach ($widgetInstance->getData('page_groups') as $group) {
                    $name = $group['page_group'];
                    $groups []= array(
                        'page_group' => $name,
                        $name => array(
                            'page_id' => $group['page_id'],
                            'layout_handle' => $group['layout_handle'],
                            'entities' => $group['entities'],
                            'for' => $group['page_for'],
                            'block' => $group['block_reference'],
                            'template' => $group['page_template']
                        )
                    );
                }
                $widgetInstance->setData('page_groups', $groups);
            }
            $widgetInstance->save();
            $response['status'] = true;
            $this->_getSession()->addSuccess($this->__('Widget instance successfully copied'));

        } catch (Exception $e) {
            $response['message'] = $e->getMessage();
        }

        $this->getResponse()->setHeader('Content-type', 'application/json')->setBody(Mage::helper('core')->jsonEncode($response));
    }
}
