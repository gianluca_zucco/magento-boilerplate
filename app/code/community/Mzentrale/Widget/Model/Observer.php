<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Model_Observer
{
    /**
     * Adds specific layout handle in admin under widget instance edit action to better manage
     * tabs within layout file
     *
     * @param Varien_Event_Observer $observer
     */
    public function addCustomWidgetLayoutHandles(Varien_Event_Observer $observer)
    {
        $widget = Mage::registry('current_widget_instance');
        /** @var Mage_Adminhtml_Controller_Action $action */
        $action = $observer->getAction();

        if ($widget instanceof Mage_Widget_Model_Widget_Instance && $action->getFullActionName() == 'adminhtml_widget_instance_edit') {
            $observer->getLayout()->getUpdate()->addHandle(str_replace('/', '_',$widget->getType()));
        }
    }

    public function forceAllowUseUrlsInFrontend(Varien_Event_Observer $observer)
    {
        $observer->getResult()->isAllowed = true;
    }
}
