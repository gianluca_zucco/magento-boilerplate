<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Model_Source_Store extends Mage_Adminhtml_Model_System_Config_Source_Store
{
    public function toOptionGroupArray()
    {
        /**
         * @var Mage_Core_Model_Website $website
         * @var Mage_Core_Model_Store $store
         */
        $options = array();

        foreach (Mage::getResourceModel('core/website_collection') as $website) {

            $optgroup = array(
                'label' => $website->getName(),
                'value' => array()
            );

            foreach ($website->getStores() as $store)   {
                array_push($optgroup['value'], array(
                    'label' => $store->getName(),
                    'value' => $store->getId()
                ));
            }

            array_push($options, $optgroup);
        }

        array_unshift($options, array(
            'label' => Mage::helper('mzentrale_widget')->__('Please select'),
            'value' => null
        ));

        return $options;
    }
}
