<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Model_Source_Columns
{
    protected $_options;

    public function toOptionArray()
    {
        if (!$this->_options)   {
            $this->_options = array(
                array(
                    'value' => 2,
                    'label' => Mage::helper('mzentrale_widget')->__('1/6')
                ),
                array(
                    'value' => 3,
                    'label' => Mage::helper('mzentrale_widget')->__('1/4')
                ),
                array(
                    'value' => 4,
                    'label' => Mage::helper('mzentrale_widget')->__('1/3')
                ),
                array(
                    'value' => 6,
                    'label' => Mage::helper('mzentrale_widget')->__('1/2')
                ),
                array(
                    'value' => 8,
                    'label' => Mage::helper('mzentrale_widget')->__('2/3')
                ),
                array(
                    'value' => 12,
                    'label' => Mage::helper('mzentrale_widget')->__('Fullwidth')
                )
            );
        }
        return $this->_options;
    }
}
