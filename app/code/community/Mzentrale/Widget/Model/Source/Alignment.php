<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Model_Source_Alignment
{
    protected $_options;
    const ALIGN_LEFT = 'left';
    const ALIGN_CENTER = 'center';
    const ALIGN_RIGHT = 'right';


    public function toOptionArray()
    {
        if (!$this->_options)   {
            $this->_options = array(
                array(
                    'value' => self::ALIGN_LEFT,
                    'label' => Mage::helper('mzentrale_widget')->__('Left')
                ),
                array(
                    'value' => self::ALIGN_CENTER,
                    'label' => Mage::helper('mzentrale_widget')->__('Center')
                ),
                array(
                    'value' => self::ALIGN_RIGHT,
                    'label' => Mage::helper('mzentrale_widget')->__('Right')
                )
            );
        }
        return $this->_options;
    }
}
