<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Model_Source_Category
{
    public function toOptionArray()
    {
        /**
         * @var Mage_Core_Model_Website $website
         * @var Mage_Core_Model_Store $store
         */
        $options = array();

        $categories = Mage::getResourceModel('catalog/category_collection')->addNameToResult();
        $categories->addFilter('level', array('gt' => 2));
        foreach ($categories as $category)  {
            array_push($options, array(
                'label' => $category->getName(),
                'value' => $category->getId()
            ));
        }

        array_unshift($options, array(
            'label' => Mage::helper('mzentrale_widget')->__('Please select'),
            'value' => null
        ));

        return $options;
    }
}
